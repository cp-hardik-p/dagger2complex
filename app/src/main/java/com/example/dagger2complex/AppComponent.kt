package com.example.dagger2complex

import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [SharedPreferenceModule::class])
interface AppComponent {
fun inject(activity: MainActivity)
}