package com.example.dagger2complex

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class SharedPreferenceModule {
    private  var context: Context

    constructor(context: Context)
    {
        this.context = context
    }

    @Singleton
    @Provides
   open fun provideContext(): Context {
        return context
    }

    @Singleton
    @Provides
  open  fun provideSharedPreferences(context: Context): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }
}