package com.example.dagger2complex

import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {
    private lateinit var component: AppComponent

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        component = DaggerAppComponent.builder().sharedPreferenceModule(SharedPreferenceModule(this)).build()
        component.inject(this)

        btnSave.setOnClickListener {
            sharedPreferences.edit()?.putString("username", inUsername.text.toString().trim())?.apply()
            sharedPreferences.edit()?.putString("number", inNumber.text.toString().trim())?.apply()
            sharedPreferences.edit()?.apply()
            Toast.makeText(this, "saved successfully", Toast.LENGTH_SHORT).show()
        }

        btnGet.setOnClickListener {
            inUsername.setText(sharedPreferences.getString("username", "hardik"))
            inNumber.setText(sharedPreferences.getString("number", "12345"))
        }

        btnClear.setOnClickListener {
            inUsername.setText("")
            inNumber.setText("")
        }
    }
}